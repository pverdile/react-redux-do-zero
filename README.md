# Próximos passos

1. [OK] implementar o uso de middlewares na store
1. [OK] implementar uma chamada para o backend assíncrona
1. implementar o render do react do zero
1. passar a store por Context API
1. exercitar a atualização de componentes do react após a mudança do estado

# Referências

1. [ReduxJS](https://redux.js.org/)
1. [ReactReduxJS](https://react-redux.js.org/)
