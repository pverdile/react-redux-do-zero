export function createStore(reducers, enhancer) {
  let currentState = {};
  let currentListener;

  if (enhancer !== undefined) {
    return enhancer(createStore)(reducers);
  }

  function getState() {
    return currentState;
  }

  function dispatch(action) {
    Object.entries(reducers).forEach(([name, reducer]) => {
      currentState[name] = reducer(currentState[name], action);
    });

    if (currentListener) {
      currentListener();
    }
  }

  function subscribe(listener) {
    currentListener = listener;
  }

  reducers && dispatch({ type: "INIT" });

  return {
    getState,
    dispatch,
    subscribe,
  };
}
