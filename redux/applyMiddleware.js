export function applyMiddleware(middleware) {
  return function applyMiddlewareEnhancer(createStore) {
    return function applyMiddlewareReducer(reducers) {
      const store = createStore(reducers);

      const middlewareAPI = {
        getState: store.getState,
        dispatch: store.dispatch,
      };

      const dispatch = middleware(middlewareAPI)(store.dispatch);

      return {
        ...store,
        dispatch,
      };
    };
  };
}
