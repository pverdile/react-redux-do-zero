import { requestBackend } from "../api/api";
import { createAction } from "../createAction";

export const chamadaAssíncrona = createAction(
  "CHAMADA_ASSINCRONA",
  (payload) => ({
    payload,
    api: requestBackend,
  })
);
