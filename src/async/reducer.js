const initialState = { sum: 0, status: "idle" };

export function reducerAsync(state = initialState, action) {
  switch (action.type) {
    case "CHAMADA_ASSINCRONA REQUESTED":
      return { ...state, status: "pending" };
    case "CHAMADA_ASSINCRONA COMPLETED":
      return { sum: state.sum + action.payload, status: "completed" };
    default:
      return state;
  }
}
