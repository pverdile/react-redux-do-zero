export function asyncMiddleware({ getState, dispatch }) {
  return function (next) {
    return async function (action) {
      if (action.api) {
        const { api, payload, type } = action;

        dispatch({ type: `${type} REQUESTED`, payload });

        const response = await api(payload);

        dispatch({
          type: `${type} COMPLETED`,
          meta: action,
          payload: response,
        });

        return;
      }

      return next(action);
    };
  };
}
