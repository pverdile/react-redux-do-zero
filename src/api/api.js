export function requestBackend(payload) {
  return new Promise((res) => {
    setTimeout(() => {
      res(payload * 2);
    }, 5000);
  });
}
