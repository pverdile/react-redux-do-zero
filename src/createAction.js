import { store } from "./store";

export const createAction = (type, actionCreator) => (payload) => {
  const action = actionCreator(payload);

  action.type = type;

  return store.dispatch(action);
};
