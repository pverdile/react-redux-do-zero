import { applyMiddleware, createStore } from "../redux";
import { reducerAsync } from "./async/reducer";
import { reducerCounter1, reducerCounter2 } from "./counter/reducer";
import { addCompany, sayHi } from "./enhancers";
import { asyncMiddleware } from "./middlewares";

const enhancers = (createStore) => {
  return (reducers) => {
    const enhancer1 = sayHi(createStore);
    const enhancer2 = addCompany(enhancer1);
    const enhancer3 = applyMiddleware(asyncMiddleware)(enhancer2);

    return enhancer3(reducers);
  };
};

export const store = createStore(
  {
    counter1: reducerCounter1,
    counter2: reducerCounter2,
    async: reducerAsync,
  },
  enhancers
);

// av imirim, 3293 - 11h30
