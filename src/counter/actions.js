import { createAction } from "../createAction";

export const increment1 = createAction("INCREMENT_1", (payload) => ({
  payload,
}));

export const decrement1 = createAction("DECREMENT_1", (payload) => ({
  payload,
}));

export const increment2 = createAction("INCREMENT_2", (payload) => ({
  payload,
}));

export const decrement2 = createAction("DECREMENT_2", (payload) => ({
  payload,
}));
