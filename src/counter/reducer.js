const initialState = 0;

export function reducerCounter1(state = initialState, action) {
  switch (action.type) {
    case "INCREMENT_1":
      return state + action.payload;
    case "DECREMENT_1":
      return state - action.payload;
    default:
      return state;
  }
}

export function reducerCounter2(state = initialState, action) {
  switch (action.type) {
    case "INCREMENT_2":
      return state + action.payload;
    case "DECREMENT_2":
      return state - action.payload;
    default:
      return state;
  }
}
