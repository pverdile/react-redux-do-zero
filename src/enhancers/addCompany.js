export function addCompany(createStore) {
  return function addCompanyReducer(reducers) {
    const store = createStore(reducers);

    const newGetState = () => {
      const state = store.getState();

      const newState = { ...state, company: "AZ/Visto" };

      return newState;
    };

    return {
      ...store,
      getState: newGetState,
    };
  };
}
