export function sayHi(createStore) {
  return function sayHiReducer(reducers) {
    const store = createStore(reducers);

    const newDispatch = (action) => {
      const result = store.dispatch(action);

      console.log(action);
      console.log(store.getState());

      return result;
    };

    return {
      ...store,
      dispatch: newDispatch,
    };
  };
}
