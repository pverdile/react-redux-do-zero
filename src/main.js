import { chamadaAssíncrona } from "./async/actions";
import {
  decrement1,
  decrement2,
  increment1,
  increment2,
} from "./counter/actions";
import { store } from "./store";

// Update DOM
store.subscribe(() => {
  const state = store.getState();

  const counter1 = (state) => state.counter1;
  const counter2 = (state) => state.counter2;

  document.querySelector("#counter-1").innerHTML = counter1(state);
  document.querySelector("#counter-2").innerHTML = counter2(state);
});

// DOM listeners
document
  .querySelector("#increment-1")
  .addEventListener("click", () => increment1(1));

document.querySelector("#decrement-1").addEventListener("click", () => {
  decrement1(1);
});

document.querySelector("#increment-2").addEventListener("click", () => {
  increment2(1);
});

document.querySelector("#decrement-2").addEventListener("click", () => {
  decrement2(1);
});

document.querySelector("#chamada-async").addEventListener("click", () => {
  chamadaAssíncrona(2);
});
